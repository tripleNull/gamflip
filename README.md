# gamflip

GTK application for controlling flipping a webcam using a v4l2loopback device and ffmpeg. 

## Dependencies
	v4l2-ctl
	ffmpeg

### Required running kernel modules
	v4l2loopback


![](gamflip.png)
